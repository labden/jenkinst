docker build -t jenkins -f docker/Dockerfile.jenkins .
docker build -t jenkins-setter -f docker/Dockerfile.setter .
docker build -t jenkins-slave -f docker/Dockerfile.slave .

# clean
docker rmi -f $(docker images --quiet --filter "dangling=true")

