jenkinst
=======

#### links
- https://github.com/jenkinsci/configuration-as-code-plugin
- https://github.com/helm/charts/blob/master/stable/jenkins/values.yaml
- https://docs.openstack.org/infra/jenkins-job-builder/


#### add node
- add slave nodes in jenkins.yaml
- add slave nodes in docker-compose.yml
- add slave nodes in serve_secrets.py for secrets
- add slave nodes in build.sh



#### conventions
- visit http://127.0.0.1:8002/configuration-as-code and click view configuration
- add or edit job definitions at jobs/*
- add or edit jenkins configuration at jenkins.yaml (config as code, local development)
- add or edit jenkins configuration k8s/jenkins.yaml (helm values.yaml, production)



```bash
# compose
. build.sh
docker-compose up -d
# 127.0.0.1:8102
# 127.0.0.1:8002
# admin:admin



# kube
vagrant up
ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook playbook.yml -i inventory -l kubernetes

wget https://github.com/rancher/rke/releases/download/v1.0.0/rke_linux-amd64
chmod +x rke_linux-amd64
./rke_linux-amd64 up --config cluster.yml

export KUBECONFIG=$(pwd)/kube_config_cluster.yml
kubectl get pods --all-namespaces

# helm
kubectl apply -f create-tiller-rbac-sa.yml
helm init --service-account tiller
kubectl -n kube-system get po

# jenkins
kubectl create namespace jenkins
helm install --namespace jenkins --name jenkins stable/jenkins -f k8s/values.yaml --wait
kubectl -n jenkins create -f ingress.yml

# generate crt for nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt

docker-compose -f nginx-compose.yml up -d

echo "127.0.0.1 jenkins.rke.local" >> /etc/hosts
# https://jenkins.rke.local
# admin:admin
```
